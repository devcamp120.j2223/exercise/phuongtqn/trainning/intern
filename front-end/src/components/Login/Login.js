import { Grid, Typography, Alert, Snackbar } from "@mui/material";
import {
  Row,
  Col,
  Container,
  Input,
  Button,
  Label,
  FormGroup,
} from "reactstrap";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
function Login() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [openAlert, setOpenAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const [userName, setUserName] = useState("");
  const [passWord, setPassWord] = useState("");
  const [severity, setSeverity] = useState("");

  // Xử lý khi ấn nút lưu được các Input vào Local Storage và Dispatch vào Redux
  const onBtnSingin = () => {
    if (Validate()) {
      setOpenAlert(true);
      setSeverity("success");
      setAlertMessage("Success");
      dispatch({
        type: "USER",
        user: userName,
      });
      localStorage.setItem("username", userName);
      localStorage.setItem("password", passWord);
      navigate("/home");
    }
  };
  const Validate = () => {
    if (userName === "") {
      setOpenAlert(true);
      setSeverity("error");
      setAlertMessage("Please enter username");
      return false;
    }
    if (passWord === "") {
      setOpenAlert(true);
      setSeverity("error");
      setAlertMessage("Please provide password");
      return false;
    }
    return true;
  };
  const HandleCloseAlert = () => {
    setOpenAlert(false);
  };
  return (
    <Container fluid className="login">
      <div className="login--form">
        <FormGroup className="login--form-content">
          <Row>
            <Typography
              variant="h5"
              gutterBottom
              className="login--form-header"
            >
              Already Have Account ?
            </Typography>
          </Row>
          <Row>
            <Input
              label="Enter Username"
              placeholder="Enter Username"
              className="login--form-input"
              value={userName}
              onChange={(event) => setUserName(event.target.value)}
            />
          </Row>
          <Row>
            <Input
              type="password"
              label="Enter Password"
              className="login--form-input"
              placeholder="Enter Password"
              value={passWord}
              onChange={(event) => setPassWord(event.target.value)}
            />
          </Row>
          <Row>
            <Button className="login--form-button" onClick={onBtnSingin}>
              Sign In
            </Button>
          </Row>
          <Row className="login--form-forgot">
            <Col>
              <Input type="checkbox" />
              <Label>Remember Me</Label>
            </Col>
            <Col>
              <a href="#">Forgot Password ?</a>
            </Col>
          </Row>
          <Grid container mt={3}>
            <Grid item xs={12}>
              <Typography align="center">
                Don't have an account?
                <a href="/" style={{ textDecoration: "none" }}>
                  <Typography className="text-form" variant="subtitle">
                    {" "}
                    Sign up here
                  </Typography>
                </a>
              </Typography>
            </Grid>
          </Grid>
        </FormGroup>
        <Snackbar
          open={openAlert}
          sx={{ width: "100%" }}
          autoHideDuration={4000}
          onClose={HandleCloseAlert}
        >
          <Alert variant="filled" severity={severity}>
            {alertMessage}
          </Alert>
        </Snackbar>
      </div>
    </Container>
  );
}

export default Login;
