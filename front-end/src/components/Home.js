import { Container } from "reactstrap";
// core components
import {
  IconButton,
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
import { Row } from "reactstrap";
// Import các Components của Modal
import { AddCircleOutline, DeleteOutline, Edit } from "@mui/icons-material";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import * as React from "react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import ModalRoot from "../components/Pet_CRUD/Modal/ModalRoot";
import ModalService from "../components/Pet_CRUD/Modal/services/ModalService";
import ModalDelete from "../components/Pet_CRUD/ModalComponents/ModalDelete";
import ModalAdd from "./Pet_CRUD/ModalComponents/ModalCreate";
import EditModal from "./Pet_CRUD/ModalComponents/ModalEdit";
function Home() {
  const { user, petList, isRefresh, name } = useSelector(
    (reduxData) => reduxData.EventReducer
  );
  const [type, setType] = useState("");
  const [sex, setSex] = useState("");
  const dispatch = useDispatch();
  const openModalAdd = () => {
    ModalService.open(ModalAdd);
  };
  const openModalDelete = (row) => {
    ModalService.open(ModalDelete);
    dispatch({
      type: "ID",
      id: row.id,
      pet: row.attributes.name,
    });
  };

  const openModalEdit = (row) => {
    ModalService.open(EditModal);
    dispatch({
      type: "ID",
      id: row.id,
    });
  };
  const onChangeType = (event) => {
    setType(event.target.value);
  };
  const onChangeSex = (event) => {
    setSex(event.target.value);
  };
  // Fetch API
  const fetchAPI = async (url, body) => {
    const response = await fetch(url, body);
    const data = await response.json();
    return data;
  };

  useEffect(() => {
    fetchAPI(
      `http://localhost:1337/api/pets/?filters[name][$eq]=${name}&filters[animal][$eq]=${type}&filters[sex][$eq]=${sex}`
    )
      .then((data) => {
        dispatch({
          type: "PET_LIST",
          petList: data.data,
        });
        console.log(data);
      })
      .catch((error) => {
        console.error(error.message);
      });
    console.log(name);
  }, [name, type, sex]);

  useEffect(() => {
    if (!name && !type && !sex) {
      fetchAPI("http://localhost:1337/api/pets")
        .then((data) => {
          dispatch({
            type: "PET_LIST",
            petList: data.data,
          });
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }, [isRefresh, name, sex, type]);

  return (
    <Container fluid className="home">
      <FormControl className="filter-container" fullWidth>
        <InputLabel id="demo-simple-select-label">Type</InputLabel>
        <Select
          className="filter-input"
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Type"
          value={type}
          onChange={onChangeType}
          defaultValue="NONE"
        >
          <MenuItem value="NONE"></MenuItem>
          <MenuItem value="cat">Cat</MenuItem>
          <MenuItem value="dog">Dog</MenuItem>
        </Select>
      </FormControl>

      <FormControl className="filter-container" fullWidth>
        <InputLabel id="demo-simple-select-label">Sex</InputLabel>
        <Select
          className="filter-input"
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Sex"
          value={sex}
          defaultValue="NONE"
          onChange={onChangeSex}
        >
          <MenuItem value="NONE"></MenuItem>
          <MenuItem value="male">Male</MenuItem>
          <MenuItem value="female">Female</MenuItem>
        </Select>
      </FormControl>
      <BottomNavigation
        className="button-container"
        showLabels
        style={{ backgroundColor: "#EDEDED" }}
      >
        {user ? (
          <BottomNavigationAction
            label="Add Pet"
            value="AddPet"
            onClick={openModalAdd}
            icon={<AddCircleOutline />}
            className="btn-add"
          />
        ) : (
          <Typography className="btn-add"> ĐĂNG NHẬP ĐỂ TIẾP TỤC </Typography>
        )}
      </BottomNavigation>

      <Row
        className={
          petList.length > 0 || petList.length === 0
            ? "home--detail"
            : "empty-home"
        }
      >
        {petList.map((pet, i) => {
          return (
            <div className="Card">
              <div className="upper-container"></div>
              <div className="lower-container">
                <h3>{pet.attributes.name}</h3>
                <h5>Breed: {pet.attributes.breed}</h5>
                <h5>Animal: {pet.attributes.animal}</h5>
                <h5>Location: {pet.attributes.location} </h5>
                <h5>Age: {pet.attributes.age} </h5>
                <h5>Sex: {pet.attributes.sex} </h5>
              </div>
              <div className="footer-container">
                <IconButton edge="end" aria-label="edit">
                  {user ? (
                    <Edit
                      sx={{ color: "#8e00b9" }}
                      onClick={() => {
                        openModalEdit(pet);
                      }}
                    />
                  ) : null}
                </IconButton>

                <IconButton edge="end" aria-label="edit">
                  {user ? (
                    <DeleteOutline
                      sx={{ color: "red" }}
                      onClick={() => {
                        openModalDelete(pet);
                      }}
                    />
                  ) : null}
                </IconButton>
              </div>
            </div>
          );
        })}
      </Row>
      <ModalRoot />
    </Container>
  );
}

export default Home;
