const initialState = {
  user: null,
  id: "",
  pet: "",
  petList: [],
  isRefresh: false,
  name: "",
  sex: "",
  animal: "",
};

const ActionHandler = (state = initialState, action) => {
  switch (action.type) {
    case "REFRESH": {
      return {
        ...state,
        isRefresh: action.isRefresh,
      };
    }
    case "NAME": {
      return {
        ...state,
        name: action.name,
      };
    }
    case "SEX": {
      return {
        ...state,
        sex: action.sex,
      };
    }
    case "ANIMAL": {
      return {
        ...state,
        animal: action.animal,
      };
    }
    case "USER": {
      return {
        ...state,
        user: action.user,
      };
    }
    case "PET_LIST": {
      return {
        ...state,
        petList: [...action.petList],
      };
    }
    case "ID": {
      return {
        ...state,
        id: action.id,
        pet: action.pet,
      };
    }
    default: {
      return state;
    }
  }
};

export default ActionHandler;
