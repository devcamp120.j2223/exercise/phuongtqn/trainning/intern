import {
  Grid,
  Alert,
  Box,
  Button,
  TextField,
  FormControl,
  InputLabel,
  Select,
  Snackbar,
  Typography,
  MenuItem,
} from "@mui/material";
import { useState } from "react";
import Modal from "../Modal/Modal";
import ModalHeader from "../Modal/ModalHeader";
import ModalBody from "../Modal/ModalBody";
import ModalFooter from "../Modal/ModalFooter";
import { useDispatch, useSelector } from "react-redux";

function ModalAdd(props) {
  const { isRefresh } = useSelector((reduxData) => reduxData.EventReducer);
  const dispatch = useDispatch();
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [sex, setSex] = useState("");
  const [location, setLocation] = useState("");
  const [animal, setAnimal] = useState("");
  const [breed, setBreed] = useState("");
  const [serverity, setServerity] = useState("");
  const [alert, setAlert] = useState("");
  const [openAlert, setOpenAlert] = useState(false);

  const handleCloseAlert = () => {
    setOpenAlert(false);
  };

  const fetchAPI = async (url, body) => {
    const response = await fetch(url, body);
    const data = await response.json();
    return data;
  };
  // Handle click confirm
  const onBtnConfirmClick = () => {
    if (Validate()) {
      fetchAPI("http://localhost:1337/api/pets/", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          data: {
            name: name,
            animal: animal,
            breed: breed,
            age: age,
            sex: sex,
            location: location,
          },
        }),
      })
        .then((data) => {
          setOpenAlert(true);
          setServerity("success");
          setAlert("Create new pet successfully");
          dispatch({
            type: "REFRESH",
            isRefresh: isRefresh + 1,
          });
        })
        .catch((error) => {
          setOpenAlert(true);
          setServerity("error");
          setAlert("Create fail");
        });
    }
  };

  //Validate
  const Validate = () => {
    if (name === "") {
      setOpenAlert(true);
      setServerity("error");
      setAlert("Provide name !!");
      return false;
    }
    if (animal === "") {
      setOpenAlert(true);
      setServerity("error");
      setAlert("Provide animal !!");
      return false;
    }
    if (age === "") {
      setOpenAlert(true);
      setServerity("error");
      setAlert("Provide age !!");
      return false;
    }
    if (sex === "") {
      setOpenAlert(true);
      setServerity("error");
      setAlert("Provide sex !!");
      return false;
    }
    if (location === "") {
      setOpenAlert(true);
      setServerity("error");
      setAlert("Provide location !!");
      return false;
    }
    return true;
  };

  return (
    <>
      <Modal
        // open={openModalAdd}
        // onClose={closeModalAdd}
        aria-labelledby="modal-add"
        aria-describedby="modal-add"
      >
        <Box className="modal--style">
          <ModalHeader>
            <Typography
              mb={2}
              id="modal-modal-title"
              variant="h5"
              component="h2"
            >
              <b>Create Pet!</b>
            </Typography>
          </ModalHeader>

          <ModalBody>
            <Grid container style={{ marginTop: "50px" }}>
              <Grid container mt={2}>
                <Grid item sm={12}>
                  <Grid container>
                    <Grid item sm={3}>
                      <label>Name :</label>
                    </Grid>
                    <Grid item sm={9}>
                      <TextField
                        fullWidth
                        label="Name"
                        className="bg-white"
                        size="small"
                        value={name}
                        onChange={(event) => setName(event.target.value)}
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>

              <Grid container mt={2}>
                <Grid item sm={12}>
                  <Grid container>
                    <Grid item sm={3}>
                      <label>Animal :</label>
                    </Grid>
                    <Grid item sm={9}>
                      <FormControl fullWidth size="small">
                        <InputLabel id="demo-simple-select-label">
                          Animal
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          defaultValue="NOT"
                          value={animal}
                          label="Animal"
                          onChange={(event) => setAnimal(event.target.value)}
                        >
                          <MenuItem value={"NOT"}></MenuItem>
                          <MenuItem value={"dog"}>Dog</MenuItem>
                          <MenuItem value={"cat"}>Cat</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>

              <Grid container mt={2}>
                <Grid item sm={12}>
                  <Grid container>
                    <Grid item sm={3}>
                      <label>Breed :</label>
                    </Grid>
                    <Grid item sm={9}>
                      <TextField
                        fullWidth
                        value={breed}
                        label="Breed"
                        className="bg-white"
                        type="text"
                        size="small"
                        onChange={(event) => setBreed(event.target.value)}
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>

              <Grid container mt={2}>
                <Grid item sm={12}>
                  <Grid container>
                    <Grid item sm={3}>
                      <label>Age :</label>
                    </Grid>
                    <Grid item sm={9}>
                      <TextField
                        fullWidth
                        value={age}
                        label="Age"
                        className="bg-white"
                        type="number"
                        size="small"
                        onChange={(event) => setAge(event.target.value)}
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>

              <Grid container mt={2}>
                <Grid item sm={12}>
                  <Grid container>
                    <Grid item sm={3}>
                      <label>Sex :</label>
                    </Grid>
                    <Grid item sm={9}>
                      <FormControl fullWidth size="small">
                        <InputLabel id="demo-simple-select-label">
                          Sex
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          defaultValue="NOT"
                          value={sex}
                          label="Age"
                          onChange={(event) => setSex(event.target.value)}
                        >
                          <MenuItem value={"NOT"}></MenuItem>
                          <MenuItem value={"male"}>Male</MenuItem>
                          <MenuItem value={"female"}>Female</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>

              <Grid container mt={2}>
                <Grid item sm={12}>
                  <Grid container>
                    <Grid item sm={3}>
                      <label>Location :</label>
                    </Grid>
                    <Grid item sm={9}>
                      <TextField
                        fullWidth
                        value={location}
                        label="Location"
                        className="bg-white"
                        type="text"
                        size="small"
                        onChange={(event) => setLocation(event.target.value)}
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </ModalBody>
          <ModalFooter>
            <Grid container className="mt-4 text-center">
              <Grid item sm="12">
                <Grid container className="mt-4">
                  <Grid item sm="6">
                    <Button
                      onClick={onBtnConfirmClick}
                      className="w-100 text-white"
                      style={{ backgroundColor: "#ACB0B4" }}
                    >
                      Confirm
                    </Button>
                  </Grid>
                  <Grid item sm="6">
                    <Button
                      className="bg-danger w-75 text-white"
                      onClick={props.close}
                    >
                      Cancel
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </ModalFooter>
        </Box>
      </Modal>
      <Snackbar
        open={openAlert}
        fullWidth
        autoHideDuration={6000}
        onClose={handleCloseAlert}
      >
        <Alert
          onClose={handleCloseAlert}
          severity={serverity}
          sx={{ width: "100%" }}
        >
          {alert}
        </Alert>
      </Snackbar>
    </>
  );
}

export default ModalAdd;
