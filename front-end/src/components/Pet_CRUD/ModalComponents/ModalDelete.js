import { Alert, Button, Snackbar, Typography, Box, Grid } from "@mui/material";
import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Modal from "../Modal/Modal";
import ModalFooter from "../Modal/ModalFooter";
import ModalHeader from "../Modal/ModalHeader";
import ModalBody from "../Modal/ModalBody";

function ModalDelete(props) {
  const { id, pet, isRefresh } = useSelector(
    (reduxData) => reduxData.EventReducer
  );
  const dispatch = useDispatch();
  console.log(id);

  const [serverity, setServerity] = useState("");
  const [alert, setAlert] = useState("");
  const [openAlert, setOpenAlert] = useState(false);

  const handleCloseAlert = () => {
    setOpenAlert(false);
  };

  const onBtnConfirmDeleteClick = () => {
    console.log("%cXác nhận xóa được click!", "color:red");

    fetch("http://localhost:1337/api/pets/" + id, { method: "DELETE" })
      .then((data) => {
        console.log("Delete successful");
        setOpenAlert(true);
        setServerity("success");
        setAlert("Delete Pet" + id + " successfully!");
        dispatch({
          type: "REFRESH",
          isRefresh: !isRefresh,
        });
      })
      .catch((error) => {
        console.error("There was an error!", error);
        setOpenAlert(true);
        setServerity("error");
        setAlert("Delete Pet" + id + " fail!");
      });
  };

  return (
    <>
      <Modal
        // open={openModalDelete}
        // onClose={handleCloseDelete}
        aria-labelledby="modal-delete"
        aria-describedby="modal-delete-customer"
      >
        <Box className="modal--style">
          <ModalHeader>
            <Typography
              mb={2}
              id="modal-modal-title"
              variant="h5"
              component="h2"
            >
              <b>Delete Pet!</b>
            </Typography>
          </ModalHeader>
          <ModalBody>
            <Grid container className="mt-2">
              <Grid item xs={12} align="center">
                <h4>
                  Delete Pet :
                  <h4 style={{ color: "red", marginTop: "20px" }}>{pet} ? </h4>
                </h4>
              </Grid>
            </Grid>
          </ModalBody>
          <ModalFooter>
            <Grid container className="mt-4 text-center">
              <Grid item sm="12">
                <Grid container className="mt-4">
                  <Grid item sm="6">
                    <Button
                      onClick={onBtnConfirmDeleteClick}
                      className="bg-success w-100 text-white"
                    >
                      {" "}
                      Xác nhận{" "}
                    </Button>
                  </Grid>
                  <Grid item sm="6">
                    <Button
                      onClick={props.close}
                      className="bg-danger w-100 text-white"
                    >
                      Close Modal
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </ModalFooter>
        </Box>
      </Modal>
      <Snackbar
        open={openAlert}
        fullWidth
        autoHideDuration={6000}
        onClose={handleCloseAlert}
      >
        <Alert
          onClose={handleCloseAlert}
          severity={serverity}
          sx={{ width: "100%" }}
        >
          {alert}
        </Alert>
      </Snackbar>
    </>
  );
}

export default ModalDelete;
