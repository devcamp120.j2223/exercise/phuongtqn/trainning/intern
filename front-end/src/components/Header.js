import * as React from "react";

import { TextField } from "@mui/material";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";

import LoginIcon from "@mui/icons-material/Login";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

function Header() {
  const { user, name } = useSelector((reduxData) => reduxData.EventReducer);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const onBtnLoginClick = () => {
    navigate("/login");
  };
  const onChangeName = (event) => {
    dispatch({
      type: "NAME",
      name: event.target.value,
    });
  };
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed" style={{ backgroundColor: "#ACB0B4" }}>
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            aria-label="open drawer"
            sx={{ sm: 3 }}
          >
            {" "}
            {user ? null : <LoginIcon onClick={onBtnLoginClick} />}
          </IconButton>

          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ flexGrow: 1, display: { xs: "none", sm: "block" } }}
            style={{
              color: "black",
              fontFamily: "Roboto",
              fontWeight: "bold",
            }}
          >
            ADOPT PET
          </Typography>

          <TextField
            sx={{
              "& .MuiInputLabel-root": { color: "black" }, //styles the label
              "& .MuiOutlinedInput-root": {
                "& > fieldset": { borderColor: "black" },
              },
            }}
            label="Search..."
            variant="outlined"
            value={name}
            onChange={onChangeName}
            id="header-input"
          ></TextField>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Header;
