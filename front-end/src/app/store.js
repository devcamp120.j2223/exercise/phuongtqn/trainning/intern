import { createStore, combineReducers } from "redux";
import ActionHandler from "../components/ActionHandle";

const appReducer = combineReducers({
  EventReducer: ActionHandler,
});

const store = createStore(appReducer, undefined, undefined);

export default store;
