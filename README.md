# Pet adoption app

Getting Started

This is how you can setup my project locally.

## Installation
1. Clone the repository from 
    git clone https://gitlab.com/devcamp120.j2223/exercise/phuongtqn/trainning/intern.
2. Install the package 
    npm install
3. Start the project
    cd /pet-adoption-backend 
    then npm start
    cd /front-end 
    then npm start
